<?php

namespace Drupal\string_unique;

use Drupal\Core\Field\FieldItemList;

/**
 * Defines an item list class for unique string fields.
 */
class StringUniqueFieldItemList extends FieldItemList {

  /**
   * {@inheritdoc}
   */
  public function getConstraints() {
    $constraints = parent::getConstraints();
    $constraints[] = $this->getTypedDataManager()
      ->getValidationConstraintManager()
      ->create('UniqueField', []);

    return $constraints;
  }

}
