<?php

namespace Drupal\string_unique\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\Field\Plugin\Field\FieldType\StringItem;
use Drupal\Core\TypedData\DataDefinition;

/**
 * Defines the 'string_unique' entity field type.
 *
 * @FieldType(
 *   id = "string_unique",
 *   label = @Translation("Text Unique (plain)"),
 *   description = @Translation("A field containing a plain unique string value."),
 *   category = @Translation("Text"),
 *   list_class = "\Drupal\string_unique\StringUniqueFieldItemList",
 *   default_widget = "string_textfield",
 *   default_formatter = "string"
 * )
 */
class StringUniqueItem extends StringItem {

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    $schema = parent::schema($field_definition);
    $schema['unique keys']['value'] = ['value'];

    return $schema;
  }

//  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
//    $properties = parent::propertyDefinitions($field_definition);
//    $properties['value']->addConstraint('UniqueField', []);
//
//    return $properties;
//  }

  /**
   * {@inheritdoc}
   */
  public function getConstraints() {
    $constraints = parent::getConstraints();

//    $constraint_manager = \Drupal::typedDataManager()->getValidationConstraintManager();
//    $constraints[] = $constraint_manager->create('UniqueField', []);

    return $constraints;
  }

}
